# -*- coding: utf-8 -*-

from random import randrange, choice
import time
import unittest

import albert


class TestAlbert(unittest.TestCase):

    def test_add_persons(self):
        ''' Add a person to the address book '''

        ab = albert.AddressBook()
        self.assertEqual(len(ab.get_persons()), 0)
        person = ab.create_person('John', 'Doe')
        self.assertEqual(len(ab.get_persons()), 1)
        self.assertEqual(person.first_name, 'John')
        self.assertEqual(person.last_name, 'Doe')
        self.assertEqual(person, ab.get_persons()[0])

        for i in range(50):
            ab.add_person(AlbertFactory.generate_person())

        self.assertEqual(len(ab.get_persons()), 51)


    def test_add_group(self):
        ''' Add a group to the address book '''

        ab = albert.AddressBook()

        self.assertEqual(len(ab.get_groups()), 0)

        ab.create_group('White')
        self.assertEqual(len(ab.get_groups()), 1)

        ab.create_group('Green')
        self.assertEqual(len(ab.get_groups()), 2)

        with self.assertRaises(ValueError):
            ab.create_group('Green')
        self.assertEqual(len(ab.get_groups()), 2)

        ab.create_group('Magenta')
        self.assertEqual(len(ab.get_groups()), 3)

        self.assertEqual(ab.get_group_by_name('White').name, 'White')
        self.assertEqual(ab.get_group_by_name('Green').name, 'Green')
        self.assertEqual(ab.get_group_by_name('Magenta').name, 'Magenta')
        self.assertEqual(ab.get_group_by_name('Other'), None)


    def test_find_group_members(self):
        ''' Given a group we want to easily find its members '''

        ab = albert.AddressBook()

        person1 = ab.create_person('James', 'Brown')
        person2 = ab.create_person('Aretha', 'Franklin')
        person3 = ab.create_person('John', 'Belushi')

        group = ab.create_group('Soul People')
        group.add_persons([person1, person2, person3])

        # Put in extra groups
        ab.create_group('Other People')
        ab.create_group('Other Group')

        # Make sure we get those three members
        members = ab.get_group_members('Soul People')
        self.assertEqual(len(members), 3)
        self.assertIn(person1, members)
        self.assertIn(person2, members)
        self.assertIn(person3, members)


    def test_find_persons_groups(self):
        ''' Given a person we want to easily find the groups the person belongs to. '''

        ab = albert.AddressBook()

        person1 = ab.create_person('Wanda', 'Namera', emails=['wanda.namera@hotmail.com'])
        person2 = ab.create_person('Wanda', 'Nara')
        person3 = ab.create_person('Wanda', 'Que Vengo')
        person4 = ab.create_person('Wanda', 'La Toca', address='Nassaukade 12hs')
        person5 = ab.create_person('Juan', 'Perez')
        person6 = ab.create_person('Zaira', 'Nara', phones=['+31 6 1234 8902'])

        group1 = ab.create_group_with_persons('Minas', [person1, person2, person6])
        group2 = ab.create_group_with_persons('Percantas', [person1, person2, person4])
        group3 = ab.create_group_with_persons('Menetas', [person1, person2])
        group4 = ab.create_group_with_persons('Menetos', [person5])

        # Make sure Wanda Namera is in Minas, Percantas and Menetas
        groups_person1 = person1.get_groups()
        self.assertEqual(set(groups_person1), set([group1, group2, group3]))

        # Make sure Juan Perez is in Menetos
        groups_person5 = person5.get_groups()
        self.assertEqual(set(groups_person5), set([group4]))

        # Make sure Wanda Que Vengo is in no groups
        self.assertFalse(person3.get_groups())


    def test_find_persons_by_name(self):
        ''' Find person by name (can supply either first name, last name, or both). '''

        ab = albert.AddressBook()

        person1 = ab.create_person('Wanda', 'Namera', emails=['wanda.namera@hotmail.com'])
        person2 = ab.create_person('Wanda', 'Nara')
        person3 = ab.create_person('Wanda', 'Que Vengo')
        person4 = ab.create_person('Wanda', 'La Toca', address='Nassaukade 12hs')
        person5 = ab.create_person('Juan', 'Perez')
        person6 = ab.create_person('Zaira', 'Nara', phones=['+31 6 1234 8902'])

        found_persons = ab.find_persons_by_name(first_name='wanda')
        self.assertEqual(set(found_persons), set([person1, person2, person3, person4]))

        found_persons = ab.find_persons_by_name(first_name='Juan')
        self.assertEqual(found_persons, [person5])

        found_persons = ab.find_persons_by_name(last_name='wanda')
        self.assertEqual(found_persons, [])

        found_persons = ab.find_persons_by_name(last_name='Nara')
        self.assertEqual(set(found_persons), set([person2, person6]))

        found_persons = ab.find_persons_by_name(last_name='LaToca')
        self.assertEqual(found_persons, [person4])

        found_persons = ab.find_persons_by_name(first_name='Wanda', last_name='La Toca')
        self.assertEqual(found_persons, [person4])

        found_persons = ab.find_persons_by_name(first_name='q', last_name='e')
        self.assertEqual(found_persons, [])

        with self.assertRaises(ValueError):
            found_persons = ab.find_persons_by_name(first_name=None)


    def test_find_persons_by_email_prefix(self):
        ''' Find person by email address or prefix substring '''

        ab = albert.AddressBook()

        person1 = ab.create_person(
            first_name='Wanda',
            last_name='Namera',
            emails=['wanda.namera@gmail.com', 'abc@yahoo.com'])

        person2 = ab.create_person(
            first_name='Wanda',
            last_name='Nara',
            emails=['wanda.nara@gmail.com'])

        person3 = ab.create_person(
            first_name='Wanda',
            last_name='Lopez',
            emails=['wanda.lopez@hotmail.com', 'zed@mail.com', 'at@someemail.com'])

        person4 = ab.create_person(
            first_name='Zaira',
            last_name='Nara',
            emails=['zaira.nara@fastmail.com', 'zanara@fatmail', 'zanara@hotmail.com'])

        # test full match
        found_persons = ab.find_persons_by_email('wanda.nara@gmail.com')
        self.assertEqual(set(found_persons), set([person2]))

        # test inner substring matches
        found_persons = ab.find_persons_by_email('hotmail')
        self.assertEqual(set(found_persons), set([person3, person4]))

        found_persons = ab.find_persons_by_email('wanda')
        self.assertEqual(set(found_persons), set([person1, person2, person3]))

        found_persons = ab.find_persons_by_email('anda.nara')
        self.assertEqual(set(found_persons), set([person2]))

        found_persons = ab.find_persons_by_email('.com')
        self.assertEqual(set(found_persons), set([person1, person3, person2, person4]))

        # test less than minimum searchs string length
        # with self.assertRaises(ValueError):
            #found_persons = ab.find_persons_by_email('ze')

        # test empty match
        found_persons = ab.find_persons_by_email('notfound@nowhere.com')
        self.assertEqual(set(found_persons), set([]))

        # test multiple matches within person and single result
        found_persons = ab.find_persons_by_email('zana')
        self.assertEqual(found_persons, [person4])


    def test_find_persons_by_email_prefix_with_volume(self):
        ''' Idem but with ten thousand entries '''

        # make sure we index things up in a reasonable time
        t0 = time.time()
        ab = albert.AddressBook()
        for i in range(5000):
            ab.add_person(AlbertFactory.generate_person())
        t1 = time.time()

        print('\nIndexed {} records in {} secs'.format(
            len(ab.get_persons()),
            str(round(t1 - t0, 3))))
        self.assertLessEqual(t1 - t0, 10)

        # make sure lookups are blazing fast
        person1 = ab.create_person(
            first_name='Wanda',
            last_name='Namera',
            emails=['wanda.namera@gmail.com', 'abc@yahoo.com'])
        t0 = time.time()
        found_persons = ab.find_persons_by_email('wanda')
        self.assertIn(person1, found_persons)
        t1 = time.time()

        print('Made a single email match in {} secs'.format(t1 - t0))
        self.assertLessEqual(t1 - t0, 1)

        # index me up
        me = ab.create_person('Pablo', 'Gonzalez Portela', emails=['pablogportela@fastmail.com', 'pablo.portela@gmail.com'])
        t0 = time.time()

        # search all fastmail.com domains and make sure I am listed
        substring = 'fastmail.com'
        found_persons = ab.find_persons_by_email(substring)
        self.assertIn(me, found_persons)
        t1 = time.time()
        print('Found {} persons with email substring \'{}\' in {} secs'.format(
            len(found_persons),
            substring,
            str(round(t1 - t0, 3))))

        self.assertLessEqual(t1 - t0, 1)

    def test_get_email_substrings(self):
        self.assertEqual(
            set(albert.AddressBook.get_email_substrings('user@hm.nl')),
            set([
		'u', 's', 'e', 'r', '@', 'h', 'm', '.', 'n', 'l',
                'us', 'se', 'er', 'r@', '@h', 'hm', 'm.', '.n', 'nl',
                'use', 'ser', 'er@', 'r@h', '@hm', 'hm.', 'm.n', '.nl',
                'user', 'ser@', 'er@h', 'r@hm', '@hm.', 'hm.n', 'm.nl',
                'user@', 'ser@h', 'er@hm', 'r@hm.', '@hm.n', 'hm.nl',
                'user@h', 'ser@hm', 'er@hm.', 'r@hm.n', '@hm.nl',
                'user@hm', 'ser@hm.', 'er@hm.n', 'r@hm.nl',
                'user@hm.', 'ser@hm.n', 'er@hm.nl',
                'user@hm.n', 'ser@hm.nl',
                'user@hm.nl']))



class AlbertFactory(object):
    ''' Generate random but real looking test data '''

    first_names = ['Sandra', 'John', 'Alberto', 'Pamela', 'Peter', 'Jaap', 'Rita', 'Joe', 'Pyotr', 'Joseph']
    last_names = ['Smith', 'Johansen', 'Picasso', 'Lacroix', 'Simpson', 'Gonzalez', 'De La Vega', 'Chang', 'Moulinex']
    domains = ['gmail.com', 'fastmail.com', 'yahoo.com', 'hotmail.com', 'qmail.com', 'snail.com', 'emailing.net', 'fame.nl']

    @classmethod
    def generate_person(cls):
        first_name = choice(cls.first_names)
        last_name = choice(cls.last_names)
        p = albert.Person(first_name, last_name)
        p.emails = [cls.generate_email_for_name(first_name, last_name)]

        return p

    @classmethod
    def generate_email_for_name(cls, first_name, last_name):
        return albert.Email(
            '{}{}@{}'.format(
                cls.generate_user_for_email(first_name, last_name),
                str(randrange(1000,9999)),
                choice(cls.domains)))

    @classmethod
    def generate_user_for_email(cls, first_name, last_name):
        first_name = first_name.lower().replace(' ','')
        last_name = last_name.lower().replace(' ','')

        return first_name[:randrange(1, 3)] + last_name[:randrange(1, 5)]
