# -*- coding: utf-8 -*-

import bisect
import itertools

import trie

class AddressBook(object):
    ''' This is the class that owns the system logic and provides the api '''

    min_email_search_chars = 1

    def __init__(self):
        self.groups = dict()
        self.persons_by_first_name = dict()
        self.persons_by_last_name = dict()
        self.persons_trie = trie.LetterNode()

    def get_persons(self):
        ''' Returns all the persons '''

        # We must access the via some index
        return list(itertools.chain.from_iterable(self.persons_by_first_name.values()))

    def add_person(self, person):
        if not isinstance(person, Person):
            raise TypeError

        self.index_person(person)

    def index_person(self, person):
        ''' We index by first name, last name and email  '''

        # index first name with normalized names in dictionaries
        first_name_key = Person.normalize_name(person.first_name)
        if first_name_key not in self.persons_by_first_name:
            self.persons_by_first_name[first_name_key] = list()
        self.persons_by_first_name[first_name_key].append(person)

        # handle last name likewise
        last_name_key = Person.normalize_name(person.last_name)
        if last_name_key not in self.persons_by_last_name:
            self.persons_by_last_name[last_name_key] = list()
        self.persons_by_last_name[last_name_key].append(person)

	# handle email indexing
        for email in person.emails:
            for substring in self.get_email_substrings(email.email):
                self.persons_trie.index(substring, person)

        return person

    @classmethod
    def get_email_substrings(cls, email):
        length = len(email)
        assert(length >= cls.min_email_search_chars)

        s = list()
        j = length - cls.min_email_search_chars  + 1
        for i in range(length - cls.min_email_search_chars  + 1): # i -> 0 - 7
            for j in range(i + 1): # 10, 9
                s.append(email[j:j + length - i])

        return s

    def get_groups(self):
        return self.groups

    def add_group(self, group):
        if not isinstance(group, Group):
            raise TypeError

        self.groups.append(group)
        return group

    def create_group_with_persons(self, name, persons):
        group = self.create_group(name)
        group.add_persons(persons)
        return group

    def create_person(self, first_name, last_name, address=None, emails=None, phones=None):
        person = Person(first_name, last_name)

        if address:
            person.address = Address(address)

        if isinstance(emails, list):
            for v in emails:
                email = Email(v)
                person.emails.append(email)

        if isinstance(phones, list):
            for v in phones:
                phone = Phone(v)
                person.phones.append(phone)

        self.add_person(person)
        return person

    def create_group(self, name):
        if not isinstance(name, str):
            raise TypeError

        if self.get_group_by_name(name) is not None:
            raise ValueError

        group = Group(name)
        self.groups[name] = group
        return group

    def get_group_by_name(self, name):
        return self.groups.get(name, None)

    def get_group_members(self, name):
        if not isinstance(name, str):
            raise TypeError

        group = self.get_group_by_name(name)
        if group is None:
            raise ValueError

        return group.persons

    def find_persons_by_name(self, first_name=None, last_name=None):
        ''' Find persons via normalised first and last name hash lookups '''

        if first_name and not last_name:
            return self.find_persons_by_first_name(first_name)

        if not first_name and last_name:
            return self.find_persons_by_last_name(last_name)

        if first_name and last_name:
            # combine fields in an 'and' fashion
            return list(set.intersection(
                set(self.find_persons_by_first_name(first_name)),
                set(self.find_persons_by_last_name(last_name))))

        raise ValueError

    def find_persons_by_first_name(self, first_name):
        ''' Lookup persons via first name hash '''
        return self.persons_by_first_name.get(Person.normalize_name(first_name), list())

    def find_persons_by_last_name(self, last_name):
        ''' Lookup persons via last name hash '''

        return self.persons_by_last_name.get(Person.normalize_name(last_name), list())

    def find_persons_by_email(self, prefix):
        ''' Find the first prefix match in a sorted list, keep going while prefix matches '''

        if len(prefix) < self.min_email_search_chars:
            raise ValueError

        # use a set so we never have person dupes
        return self.persons_trie.search(prefix)


class Person(object):

    def __init__(self, first_name, last_name):
        if not isinstance(first_name, str) or not isinstance(last_name, str):
            raise TypeError

        self.first_name = first_name
        self.last_name = last_name
        self.address = None
        self.groups = list()
        self.emails = list()
        self.phones = list()

    def __str__(self):
        return '{} {}'.format(self.first_name, self.last_name)

    def get_groups(self):
        return self.groups

    @staticmethod
    def normalize_name(name):
        return name.lower().replace(' ','')


class Group(object):

    def __init__(self, name):
        self.name = name
        self.persons = list()

    def __str__(self):
        return self.name

    def get_persons(self):
        return self.persons

    def add_person(self, person):
        if not isinstance(person, Person):
            raise TypeError

        person.groups.append(self)
        self.persons.append(person)

    def add_persons(self, persons):
        for person in persons:
            self.add_person(person)


class Address(object):
    def __init__(self, line1):
        if not isinstance(line1, str):
            raise TypeError

        self.street = line1

    def __str__(self):
        return self.line1


class Email(object):
    def __init__(self, email):
        if not isinstance(email, str):
            raise TypeError

        self.email = email

    def __str__(self):
        return self.email


class Phone(object):
    def __init__(self, number):
        if not isinstance(number, str):
            raise TypeError

        self.number = number

    def __str__(self):
        return self.number
