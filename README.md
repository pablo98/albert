# README #

This is Albert, an address book API.

## Features Included ##

### Getting started ###

Import library and instantiate AddressBook

```
import albert

ab = albert.AddressBook()
```

Run tests, from project's directory

```
python -m unittest tests.TestAlbert
```

### Add a person ###

Must supply first and last name, may also supply emails, phones and street address

```
person1 = ab.create_person('John', 'Doe')
person2 = ab.create_person('Wanda', 'Namera', emails=['wanda.namera@hotmail.com'])
person3 = ab.create_person('Zaira', 'Nara', phones=['+31 6 1234 8902'])
person4 = ab.create_person('Gianni', 'Luna Dei', address='Nassaukade 12hs')
```
### Add a group to the address book ###
Must supply group name
```
group1 = ab.create_group('Wonderful Group')
```

May add members in the same step
```
group1 = ab.create_group_with_persons('Menetas', [person1, person2, person3])
```


### Find group members ###
```
members = ab.get_group_members('Menetas')
```

### Find person's groups ###
```
groups_person1 = person1.get_groups()
```

### Find person by name (can supply either first name, last name, or both). ###
```
found_persons = ab.find_persons_by_name(first_name='Wanda', last_name='La Toca')
```
Names are searched in a case insensitive way and spaces are removed


### Find person by email address, with a matching substring
```
found_persons = ab.find_persons_by_email('@company.com')
```
Will find every email containing '@company.com'

```
found_persons = ab.find_persons_by_email('john.doe@hotmail.com')
```
Will find every email containing 'john.doe@hotmail.com' (hopefully one!)


## Thoughts about arbitrary substring matching for email search ##

I have implemented such feature with trie structure

https://en.wikipedia.org/wiki/Trie
