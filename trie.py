from collections import defaultdict


class LetterNode(object):

    def __init__(self):
        self.objects = set()
        self.children = defaultdict(LetterNode)

    def index(self, word, data):
        head, tail = word[0], word[1:]
        node = self.children[head]
        node.objects.add(data)
        if tail:
            node.index(tail, data)

    def search(self, word):
        prefix_node = self._get_prefix_node(word)
        return list(prefix_node.objects) if prefix_node else []

    def _get_prefix_node(self, word):
        head, tail = word[0], word[1:]

        if head in self.children:
            if tail:
                # still traversing the prefix
                return self.children[head]._get_prefix_node(tail)
            else:
                # prefix found, return node
                return self.children[head]

        # prefix not found
        return None
